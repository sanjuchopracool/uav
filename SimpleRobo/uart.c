#include "uart.h"

void UARTInit(uint16_t ubrr)
{
     /* Set baud rate */
     UBRR0H = (unsigned char)(ubrr>>8);
     UBRR0L = (unsigned char)ubrr;

     /* Enable  transmitter */

     UCSR0B = (1<<TXEN0);

     /* Set frame format: 8data, 2stop bit
     URSEL to slect UCSRC register

     UMSEL 0 for asynchronous operation and 1 for synchronous operation

     USBS for Stop Bits 0 for one stop bits 1 for 2 stop bits

     UCSZ[2:0]: 000,5bits;001,6 bits;010,7 bits;011,8 bits;111,9 bits

     UPM[1:0]:00 ,no parity;10 even parity ;11 odd parity
     */

     UCSR0C = (3<<UCSZ00);

     // initialise tx head and tail to zero
     TX_HEAD =0;
     TX_TAIL =0;
     // enable global interrupts
     sei();
}
void QueueByte(unsigned char data)
{
    unsigned char tmphead ;
    //calculate buffer index
    tmphead = (TX_HEAD + 1) & BUFF_TX_MASK ;
    // wait for the buffer to empty
    while(tmphead== TX_TAIL) ;
    UART_TX_BUFF[tmphead] =data; //store data in the buffer
    TX_HEAD =tmphead;            //store the index

    // make sure that UDRE  interrupt is enabled
    UCSR0B |= (1<<UDRIE0) ;
}
ISR(USART_UDRE_vect)
{
    //check if the bytes are available in buffer
    if(TX_TAIL != TX_HEAD)
    {
        TX_TAIL = (TX_TAIL + 1) & BUFF_TX_MASK ;
        UDR0 =UART_TX_BUFF[TX_TAIL] ;   //transmit data from buffer
    }
    // else disable the  UDRE interrupt
    else
    UCSR0B &= ~(1<<UDRIE0) ;
}
void SendInt(int32_t val)
{
    if(val == 0)
        return;

    if(val < 0)
    {
        val=val*(-1);
        QueueByte('-');
    }
    int8_t i=7;
    char data[8]={0};
    while(val > 0)
    {
        data[i--]= val%10 + (int)'0';
        val /=10;
    }
    for(;i<8;i++)
    {
        QueueByte(data[i]);
    }
//    QueueByte('\0');
//    char str[5];
//    sprintf(str, "%d", val);
//    SendString(str);
}
void SendString(const char *msg)
{
     while(*msg!='\0')
 {
	QueueByte(*msg);
	msg++;
 }
}
void delay(int msec)
{
    int i=0 ;
    for(;i<msec;i++)
    _delay_ms(1);
}
