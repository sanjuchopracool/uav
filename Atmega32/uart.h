#include<avr/io.h>
#include<util/delay.h>
#include<avr/interrupt.h>
#define BUFSIZE 64
volatile static unsigned char rx_buf[BUFSIZE]; // Receive buffer.
volatile static unsigned char tx_buf[BUFSIZE]; // Transmit buffer.
volatile static unsigned char rx_head = 0; //!< RX buffer insert index.
volatile static unsigned char rx_tail = 0; //!< RX buffer fetch index.
volatile static unsigned char tx_head = 0; //!< TX buffer insert index.
volatile static unsigned char tx_tail = 0; //!< TX buffer fetch index.
void queue_byte( unsigned char val )
{
	tx_buf[tx_head++] = val; // Put value in buffer.
	tx_head &= BUFSIZE-1; // Wrap head index if necessary.
	UCSRB |= (1<<UDRIE); // Make sure UDRE interrupt is enabled.
}
unsigned char fetch_byte()
{
	unsigned char val;
	do {} while( rx_tail == rx_head ); // Wait for byte to arrive.
	val = rx_buf[rx_tail++]; // Store value.
	rx_tail &= BUFSIZE-1; // Wrap the tail index if necessary.
	return val;
}
ISR(USART_UDRE_vect)
{
	UDR = tx_buf[tx_tail++]; // Send value.
	tx_tail &= BUFSIZE-1; // Wrap tail index if necessary.
	if( tx_tail == tx_head ) {
		UCSRB &= ~(1<<UDRIE); // Disable interrupt if buffer empty.
	}
}
ISR(USART_RXC_vect)
{
	rx_buf[rx_head++] = UDR; // Put value in buffer.
	rx_head &= BUFSIZE-1; // Wrap head index if necessary.
}
unsigned char bytesAvailable()
{
    if(rx_head >= rx_tail)
		return (rx_head - rx_tail);
	else
		return (BUFSIZE - rx_tail + rx_head);
}

void readBuffer(unsigned char* buff, uint8_t length)
{
    //this will return the true if command was parsed otherwise noting, will write two bytes in buff
	int i = 0;
	for( ; i < length ; i++)
	{
		buff[i] = rx_buf[rx_tail++];
		rx_tail &= (BUFSIZE - 1);
	}
}

unsigned char readCommand(unsigned char* buff)
{
    while(!bytesAvailable());
    unsigned char false = 0;
    readBuffer(buff,1);
    if(buff[0] == 'A')
    {
        while(!bytesAvailable());
        readBuffer(buff,1);
        if(buff[0] == 'A')
        {
            while(bytesAvailable() < 2);
            readBuffer(buff, 2);
            return 1;
        }
        else
        {
            queue_byte('2');
            return false;
        }
    }
    {
        queue_byte('1');
        return false;
    }
}

void USARTInit(uint16_t ubrrvalue)
{
	//Set Baud rate
	UBRRH=(unsigned char)(ubrrvalue>>8);
	UBRRL=(unsigned char)ubrrvalue;
	/*Set Frame Format
	Asynchronous mode
	No Parity
	1 StopBit
	char size 8

	*/

	UCSRC =(1<<URSEL)|(1<<UCSZ1)|(1<<UCSZ0) ;

	/*Enable Interrupts
	RXCIE- Receive complete
	UDRIE- Data register empty

	Enable The recevier and transmitter

	*/

	UCSRB=(1<<RXCIE)|(1<<RXEN)|(1<<TXEN)|(1<<UDRIE);
	sei();

}

void delay(uint16_t ms)
{
    uint16_t i;
    for(i = 0; i < ms; i++)
        _delay_ms(1);
}
