#ifndef ARROWWIDGET_H
#define ARROWWIDGET_H

#include <QWidget>
#include <QTimer>
class ArrowWidget : public QWidget
{
    Q_OBJECT
    

public:
    enum MovingDirection
    {
        GoLeft,
        GoRight,
        GoStraight,
        GoBack,
        Stop
    };

    ArrowWidget(QWidget *parent = 0);
    ~ArrowWidget();

    MovingDirection arrowDirection() const;
    void setArrowDirection(ArrowWidget::MovingDirection direction);

signals:
    void directionChanged(ArrowWidget::MovingDirection);
protected:
    int heightForWidth(int val) const;
    QSize sizeHint() const;
    void paintEvent(QPaintEvent *ev);

    void keyPressEvent(QKeyEvent *ev);
    void keyReleaseEvent(QKeyEvent *ev);
private slots:
    void onTimeout();

private:
    int keyForSetDirection() const;
    void resetTimer();

private:
    MovingDirection m_direction;
    QTimer* m_timer;
    int64_t m_counter;
    int64_t m_oldCounter;
};

#endif // ARROWWIDGET_H
