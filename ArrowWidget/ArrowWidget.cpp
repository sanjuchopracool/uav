#include "ArrowWidget.h"
#include <QPainter>
#include <QPaintEvent>
#include <QKeyEvent>
#include <QDebug>

ArrowWidget::ArrowWidget(QWidget *parent)
    : QWidget(parent), m_direction(Stop)
{
    this->setMinimumSize(100, 100);
    m_counter = 0;
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    this->setFocusPolicy(Qt::StrongFocus);
}

ArrowWidget::~ArrowWidget()
{
    delete m_timer;
}

ArrowWidget::MovingDirection ArrowWidget::arrowDirection() const
{
    return m_direction;
}

void ArrowWidget::setArrowDirection(ArrowWidget::MovingDirection direction)
{
    m_direction = direction;
    emit directionChanged(m_direction);
}

int ArrowWidget::heightForWidth(int val) const
{
    return val;
}

QSize ArrowWidget::sizeHint() const
{
    return QSize(100, 100);
}

void ArrowWidget::paintEvent(QPaintEvent* ev)
{
    Q_UNUSED(ev);
    QPainter painter(this);
    int heightUnit = this->height()/5;
    int widthUnit = this->width()/5;
    int unit = heightUnit;
    if(widthUnit < heightUnit)
    {
        unit = widthUnit;
        int offset = (this->height() - 5*widthUnit)/2;
        painter.translate(0, offset);
    }
    else
    {
        unit = heightUnit;
        int offset = (this->width() - 5*heightUnit)/2;
        painter.translate(offset, 0);
    }

    painter.save();
    QPen pen;
    if(this->hasFocus())
        pen.setColor(Qt::blue);

    pen.setWidth(2);
    painter.setPen(pen);

    painter.drawRect(2*unit,0, unit, unit*5);
    painter.drawRect(0,2*unit, 5*unit, unit);
    painter.restore();

    painter.save();
    QPen transParentPen;
    transParentPen.setColor(Qt::transparent);
    painter.setPen(transParentPen);
    painter.setBrush(QColor(123, 234, 121));
    switch (m_direction)
    {
    case GoLeft:
        painter.drawRect(0, 2*unit, 2*unit, unit);
        break;
    case GoRight:
        painter.drawRect(3*unit, 2*unit, 2*unit, unit);
        break;
    case GoStraight:
        painter.drawRect(2*unit, 0, unit, 2*unit);
        break;
    case GoBack:
        painter.drawRect(2*unit, 3*unit, unit, 2*unit);
        break;
    case Stop:
        painter.save();
        painter.setBrush(Qt::darkGray);
        painter.drawRect(2*unit, 2*unit, unit, unit);
        painter.restore();
        break;
    default:
        break;
    }
    painter.restore();
}

void ArrowWidget::keyPressEvent(QKeyEvent *ev)
{
    int key = ev->key();
    if(key != keyForSetDirection())
    {
        switch (key)
        {
        case Qt::Key_Up:
            setArrowDirection(GoStraight);
            break;
        case Qt::Key_Down:
            setArrowDirection(GoBack);
            break;
        case Qt::Key_Left:
            setArrowDirection(GoLeft);
            break;
        case Qt::Key_Right:
            setArrowDirection(GoRight);
            break;
        default:
            break;
        }
        this->update();
        resetTimer();
        m_timer->start(100);
    }
    QWidget::keyPressEvent(ev);
}

void ArrowWidget::keyReleaseEvent(QKeyEvent *ev)
{
    int key = ev->key();
    switch (key)
    {
    case Qt::Key_Up:
    case Qt::Key_Down:
    case Qt::Key_Left:
    case Qt::Key_Right:
        m_counter++;
    default:
        break;
    }

    QWidget::keyReleaseEvent(ev);
}

void ArrowWidget::onTimeout()
{
    if(!m_counter)
        return;

    if(m_counter > m_oldCounter)
        m_oldCounter = m_counter;
    else
    {
        m_timer->stop();
        setArrowDirection(Stop);
        this->update();
    }

}

int ArrowWidget::keyForSetDirection() const
{
    switch (m_direction)
    {
    case GoStraight:
        return Qt::Key_Up;
    case GoBack:
        return Qt::Key_Down;
    case GoLeft:
        return Qt::Key_Left;
    case GoRight:
        return Qt::Key_Right;
    default:
        return -1;
    }
}

void ArrowWidget::resetTimer()
{
    m_counter = 0;
    m_oldCounter = 0;
}
