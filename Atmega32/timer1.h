#ifndef TIMER1_H
#define TIMER1_H
#include<avr/io.h>
extern void InitPwm1()  ; //Initialise PWM through channel 1
extern void SetPwm1A(uint8_t duty) ;//set pwm through channel OC1A
extern void SetPwm1B(uint8_t duty) ;//set pwm through channel OC1A
#endif
