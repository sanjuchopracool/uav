#include "MainWindow.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include "Command.h"


MainWindow::MainWindow(QWidget* parent) :
    QWidget(parent)
{
    this->setWindowTitle(tr("Basic Control"));
    m_comboBox = new QComboBox(this);
    m_refreshButton = new QPushButton(tr("Refresh ports list"));
    m_button = new QPushButton(tr("Open/Close"));
    m_button->setCheckable(true);

    QVBoxLayout* verticalLayout = new QVBoxLayout();
    verticalLayout->addWidget(m_comboBox);
    verticalLayout->addWidget(m_refreshButton);
    verticalLayout->addWidget(m_button);
    verticalLayout->addStretch();

    m_arrowWidget = new ArrowWidget(this);
    m_slider = new QSlider(Qt::Vertical, this);
    m_slider->setTracking(false);
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->addLayout(verticalLayout);
    layout->addWidget(m_arrowWidget);
    layout->addWidget(m_slider);
    m_slider->setRange(0, 255);
    this->setLayout(layout);
    m_arrowWidget->setFocus();

    m_port = new QSerialPort(this);
    connect(m_port, SIGNAL(readyRead()), this, SLOT(onDataReady()));
    connect(m_slider, SIGNAL(valueChanged(int)), this, SLOT(onSliderReleased()));
    connect(m_refreshButton, SIGNAL(clicked()), this, SLOT(onRefreshClicked()));
    connect(m_button, SIGNAL(toggled(bool)), this, SLOT(onPortButtonToggled(bool)));
    connect(m_arrowWidget, SIGNAL(directionChanged(ArrowWidget::MovingDirection)), this, SLOT(onDirectionChanged(ArrowWidget::MovingDirection)));
}

void MainWindow::onSliderReleased()
{
    if(m_port->isOpen())
    {
        qDebug() << "sending command to change the Speed" << m_slider->value();
        QByteArray data = prepareTransactionForSpeed((unsigned char)m_slider->value());
        m_port->write(data.data(), data.size());
    }
}

void MainWindow::onRefreshClicked()
{
    m_ports = QSerialPortInfo::availablePorts();
    m_comboBox->clear();
    Q_FOREACH(QSerialPortInfo info, m_ports)
        m_comboBox->addItem(info.portName());
}

void MainWindow::onPortButtonToggled(bool val)
{
    int index = m_comboBox->currentIndex() ;
    if(m_comboBox->count() && index < m_ports.count())
    {
        m_comboBox->setDisabled(val);
        m_refreshButton->setDisabled(val);
        if(val)
        {
            m_port->setPort(m_ports.at(index));
            m_port->open(QSerialPort::ReadWrite);
            m_arrowWidget->setFocus();
        }
        else
            m_port->close();
    }
}

void MainWindow::onDataReady()
{
    QByteArray data =  m_port->readAll();
    qDebug() << data;
    return;

    char* ptr = data.data();
    int count = data.count();
    for(int i = 0 ; i < count; i++)
        qDebug() << (int)ptr[i];
}

void MainWindow::onDirectionChanged(ArrowWidget::MovingDirection direction)
{
    if(m_port->isOpen())
    {
        qDebug() << "SendCommand to change direction" << direction;
        QByteArray data = prepareTransactionForDirection(direction);
        m_port->write(data.data(), data.size());
    }
}
