#-------------------------------------------------
#
# Project created by QtCreator 2013-10-24T17:47:36
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RoboControl
TEMPLATE = app


SOURCES += main.cpp\
        ArrowWidget.cpp \
    MainWindow.cpp

HEADERS  += ArrowWidget.h \
    MainWindow.h \
    Command.h
