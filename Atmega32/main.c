#include "uart.h"
#include "timer1.h"
void goBack()
{
    queue_byte('A');
    PORTC &= 0xF0;
    PORTC |= 0xF5;

}
void goStraight()
{
    queue_byte('B');
    PORTC &= 0xF0;
    PORTC |= 0xFA;

}

void goRight()
{
    queue_byte('R');
    PORTC &= 0xF0;
    PORTC |= 0xF9;
}

void goLeft()
{
    queue_byte('L');
    PORTC &= 0xF0;
    PORTC |= 0xF6;
}
void stop()
{
    queue_byte('S');
    PORTC &= 0xF0;
}

void changeDirection(unsigned char val)
{
    switch (val)
    {
    case 0:
        goLeft();
        break;
    case 1:
        goRight();
        break;
    case 2:
        goStraight();
        break;
    case 3:
        goBack();
        break;
    case 4:
        stop();
        break;
    default:
        break;
    }
}
void toggleLed(uint8_t toggle)
{
    if(toggle)
        PORTD &= 0x7F;
    else
        PORTD |= 0x80;
}
void main(void)
{
    USARTInit(103) ; //initialse at 4800bps for 1 Mhx at UBRR val = 12
    DDRC = 0xFF;
    DDRD |= 0x80;
    InitPwm1();
    SetPwm1A(0);
    SetPwm1B(0);
    goStraight();
    uint8_t num = 0;
    unsigned char buff[10];
    while(1)
	{
//	    delay(300);
//	    queue_byte('S');
//	    queue_byte('A');
//	    queue_byte('N');
//	    queue_byte('\n');

//    if(bytesAvailable())
//    {
//        readBuffer(buff, 1);
//        if(buff[0] == 'A')
//        {
//            num = ~num;
//            toggleLed(num);
//        }
//    }

//        num = bytesAvailable();
//        if(num)
//        {
//
//            if(num > 10)
//                num = 10;
//            readBuffer(buff, num);
//            uint8_t i;
//            queue_byte('[');
//            for(i = 0; i < num; i++)
//                queue_byte(buff[i]);
//            queue_byte(']');
//        }
        if(readCommand(buff))
        {
            switch(buff[0])
            {
            case 'D':
                changeDirection(buff[1]);
                break;
            case 'S':
                SetPwm1A(buff[1]);
                SetPwm1B(buff[1]);
                break;
            default:
                break;
            }
        }
    }
}





