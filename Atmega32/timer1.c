#include<avr/io.h>
void InitPwm1()
{
    //fast Pwm  WGM1 =1110
    //non inverted COM1A  =2  COM1B=2
    //clock prescaler =64  CS1 = 3
    TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<WGM11) ;
    TCCR1B = (1<<WGM13)|(1<<WGM12)|(1<<CS10); //|(1<<CS10) ;
    /* timer1 will also work as timer0 and timer 2 by input capture mode*/
    ICR1 = 255; //so that it will set OCR1A and B at 255  count
    DDRD |= 0x30 ; // output pin for OCR1B and OCR1A respectively PB1, PB2 in ATMEGA8
}
void SetPwm1A(uint8_t duty)
{
    OCR1A = duty ; //set value with compare register
}
void SetPwm1B(uint8_t duty)
{
    OCR1B = duty ; //set value with compare register
}

