#ifndef COMMAND_H
#define COMMAND_H
#include "ArrowWidget.h"

QByteArray prepareTransactionForDirection(ArrowWidget::MovingDirection direction) {
    QByteArray data;
    data.append("AA"); //for everycommand;
    data.append('D');
    data.append(direction);

    return data;
}

QByteArray prepareTransactionForSpeed(unsigned char val) {
    QByteArray data;
    data.append("AA"); //for everycommand;
    data.append('S');
    data.append(val);
    return data;
}

#endif // COMMAND_H
