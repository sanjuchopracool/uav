#include <QApplication>
#include "MainWindow.h"
#include <QStyleFactory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.resize(500, 300);
    w.show();
    a.setStyle(QStyleFactory::create("GTK+"));
    return a.exec();
}
