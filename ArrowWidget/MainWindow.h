#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QSlider>
#include "ArrowWidget.h"
#include <QComboBox>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QPushButton>


class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);

signals:

private slots:
    void onSliderReleased();
    void onRefreshClicked();
    void onPortButtonToggled(bool val);
    void onDataReady();
    void onDirectionChanged(ArrowWidget::MovingDirection direction);

private:
    QSlider* m_slider;
    ArrowWidget* m_arrowWidget;
    QList<QSerialPortInfo> m_ports;
    QSerialPort* m_port;
    QComboBox* m_comboBox;
    QPushButton* m_refreshButton;
    QPushButton* m_button;
};

#endif // MAINWINDOW_H
