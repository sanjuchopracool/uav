#ifndef UART_H
#define UART_H

/*  CPU   ATMEGA 16
    CLOCK  16MHz

#define FOSC 16000000// Clock Speed
#define BAUD 3840000     //Baud rate
#define MYUBRR FOSC/16/BAUD-1
*/
//  Include the headers
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//  UART Transmission buffersize ;
//  make sure size is power of 2 ,otherwise  create problem in mask operation

#define   BUFF_TX_SIZE 64

//    define Buff Mask

#define   BUFF_TX_MASK  (BUFF_TX_SIZE -1)

#if(BUFF_TX_SIZE &BUFF_TX_MASK)
     #error  BUFF_TX_SIZE is not power of 2
     #endif
#if(BUFF_TX_SIZE >256)
     #error  BUFF_TX_SIZE is greater than 256
     #endif
//static variables    buffer for sending  and  tx tail and tx head for circular queue

static unsigned char UART_TX_BUFF[BUFF_TX_SIZE];
static volatile unsigned char TX_TAIL ;
static volatile unsigned char TX_HEAD ;

// UART Functions for transmission
void UARTInit(uint16_t ubrr);
void QueueByte(unsigned char data);
void SendInt(int32_t val);
void SendString(const char *msg);
void delay(int msec);

#endif // UART_H

